//
//  AddOrEditViewController.h
//  Contact App With Core Data
//
//  Created by Alex Brown on 7/8/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AddOrEditViewController : ViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate,  UITextFieldDelegate>
{
    
    UITextField* nameTxt;
    UITextField* phoneNumberTxt;
    UITextField* emailTxt;
    UITextField* addressTxt;
    UITextField* address2Txt;
    UITextField* cityTxt;
    UITextField* stateTxt;
    UITextField* zipTxt;
    
    
}

@end



