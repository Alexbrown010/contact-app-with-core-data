//
//  ViewController.h
//  Contact App With Core Data
//
//  Created by Alex Brown on 7/8/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

{
        NSManagedObject* object;
        NSArray* fetchedObjects;
        AppDelegate *appDelegate;
        NSManagedObjectContext *context;

}

@property (strong, nonatomic) NSMutableArray *name;
@property (strong, nonatomic) NSMutableArray *phone_number;
@property (strong, nonatomic) NSMutableArray *email;
@property (strong, nonatomic) NSMutableArray *address;
@property (strong, nonatomic) NSMutableArray *image;

@property (strong, nonatomic) UITableView *tableview;


@end

