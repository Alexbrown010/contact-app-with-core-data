//
//  ProfileViewController.h
//  Contact App With Core Data
//
//  Created by Alex Brown on 7/9/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MapKit/MapKit.h>

@interface ProfileViewController : ViewController <UINavigationControllerDelegate, MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate, MKMapViewDelegate>
{
    NSArray* infoArray;
}

@property (nonatomic, retain) CLLocationManager* locationManager;

@end
