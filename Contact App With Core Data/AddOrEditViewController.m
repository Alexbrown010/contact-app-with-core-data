//
//  AddOrEditViewController.m
//  Contact App With Core Data
//
//  Created by Alex Brown on 7/8/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "AddOrEditViewController.h"

@interface AddOrEditViewController ()

@end

@implementation AddOrEditViewController

UIGestureRecognizer *tapper;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.name = [[NSMutableArray alloc] init];
    self.phone_number = [[NSMutableArray alloc] init];
    self.email = [[NSMutableArray alloc] init];
    self.address = [[NSMutableArray alloc] init];
    self.image = [[NSMutableArray alloc] init];
    
    UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:whiteView];
    
    //Dismiss keyboard
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];

    self.view.backgroundColor = [UIColor blueColor];
    self.navigationItem.rightBarButtonItem = nil;
    
    nameTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 75, self.view.frame.size.width - 20, 30)];
    nameTxt.textAlignment = NSTextAlignmentCenter;
    nameTxt.placeholder = @"-Full Name-";
    [nameTxt setBorderStyle:UITextBorderStyleBezel];
    [self.view addSubview:nameTxt];
    
    phoneNumberTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 115, self.view.frame.size.width - 20, 30)];
    phoneNumberTxt.textAlignment = NSTextAlignmentCenter;
    phoneNumberTxt.placeholder = @"-Telephone Number-";
    [phoneNumberTxt setBorderStyle:UITextBorderStyleBezel];
    [self.view addSubview:phoneNumberTxt];
    
    emailTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 155, self.view.frame.size.width - 20, 30)];
    emailTxt.textAlignment = NSTextAlignmentCenter;
    emailTxt.placeholder = @"-Email-";
    [emailTxt setBorderStyle:UITextBorderStyleBezel];
    [self.view addSubview:emailTxt];
    
    addressTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 195, self.view.frame.size.width - 20, 30)];
    addressTxt.textAlignment = NSTextAlignmentCenter;
    addressTxt.placeholder = @"-Address #1-";
    [addressTxt setBorderStyle:UITextBorderStyleBezel];
    [self.view addSubview:addressTxt];
    
    address2Txt = [[UITextField alloc] initWithFrame:CGRectMake(10, 235, self.view.frame.size.width - 20, 30)];
    address2Txt.textAlignment = NSTextAlignmentCenter;
    address2Txt.placeholder = @"-Address #2-";
    [address2Txt setBorderStyle:UITextBorderStyleBezel];
    [self.view addSubview:address2Txt];
   
    cityTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 275, self.view.frame.size.width - 20, 30)];
    cityTxt.textAlignment = NSTextAlignmentCenter;
    cityTxt.placeholder = @"-City-";
    [cityTxt setBorderStyle:UITextBorderStyleBezel];
    [self.view addSubview:cityTxt];
    
    stateTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 315, self.view.frame.size.width - 20, 30)];
    stateTxt.textAlignment = NSTextAlignmentCenter;
    stateTxt.placeholder = @"-State-";
    [stateTxt setBorderStyle:UITextBorderStyleBezel];
    [self.view addSubview:stateTxt];
    
    zipTxt = [[UITextField alloc] initWithFrame:CGRectMake(10, 355, self.view.frame.size.width - 20, 30)];
    zipTxt.textAlignment = NSTextAlignmentCenter;
    zipTxt.placeholder = @"-Postal Code-";
    [zipTxt setBorderStyle:UITextBorderStyleBezel];
    zipTxt.delegate = self;
    [self.view addSubview:zipTxt];
    
    //Link textFields through return btn
    [nameTxt addTarget:phoneNumberTxt action:@selector(becomeFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [phoneNumberTxt addTarget:emailTxt action:@selector(becomeFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [emailTxt addTarget:addressTxt action:@selector(becomeFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [addressTxt addTarget:address2Txt action:@selector(becomeFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [address2Txt addTarget:cityTxt action:@selector(becomeFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [cityTxt addTarget:stateTxt action:@selector(becomeFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    [stateTxt addTarget:zipTxt action:@selector(becomeFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    UIButton* selectPhotoBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [selectPhotoBtn addTarget:self action:@selector(addImage:)
               forControlEvents:UIControlEventTouchUpInside];
        [selectPhotoBtn setTitle:@"Add Image" forState:UIControlStateNormal];
        //[submitProfileBtn setBackgroundColor:[UIColor grayColor]];
        [[selectPhotoBtn layer] setBorderWidth:2.0f];
        [[selectPhotoBtn layer] setBorderColor:[UIColor blueColor].CGColor];
        selectPhotoBtn.frame = CGRectMake(30, 405, self.view.frame.size.width -60, 40);
        [self.view addSubview:selectPhotoBtn];
    
    UIButton* submitProfileBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [submitProfileBtn addTarget:self action:@selector(submitProfile:)
        forControlEvents:UIControlEventTouchUpInside];
        [submitProfileBtn setTitle:@"Submit Profile" forState:UIControlStateNormal];
        //[submitProfileBtn setBackgroundColor:[UIColor grayColor]];
        [[submitProfileBtn layer] setBorderWidth:2.0f];
        [[submitProfileBtn layer] setBorderColor:[UIColor blueColor].CGColor];
        submitProfileBtn.frame = CGRectMake(30, 455, self.view.frame.size.width -60, 40);
        [self.view addSubview:submitProfileBtn];
}

//Submit profile
-(void)submitProfile:(id) sender
{
    //Getting our app delegate
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    //Creating a person object
    context = [appDelegate managedObjectContext];
    NSManagedObject *person = [NSEntityDescription
                               insertNewObjectForEntityForName:@"Contact"
                               inManagedObjectContext:context];
    [person setValue:nameTxt.text forKey:@"name"];
    [person setValue:addressTxt.text forKey:@"address"];
    [person setValue:emailTxt.text forKey:@"email"];
    [person setValue:phoneNumberTxt.text forKey:@"phone_number"];
    
    //Saving person object
    NSError *error;
    BOOL saveSucceeded = [context save:&error];
    if (!saveSucceeded) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    //Feting our person objects
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Contact" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
    //Looping through and displaying the persons name
//    for (NSManagedObject *info in fetchedObjects) {
//        NSLog(@"Name: %@", [info valueForKey:@"name"]);
//        person = info;
    
//    }
//
////        //Editing an object
//                [info setValue:@"my name" forKey:@"name"];
//        
//                if (![context save:&error]) {
//                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
//                }
//    }
//
//    
////    //Fetching all objects and printing and object
//        fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
//        for (NSManagedObject *info in fetchedObjects) {
//            NSLog(@"Name: %@", [info valueForKey:@"name"]);
//        }
    
    [self.navigationController popViewControllerAnimated:YES];
}

//Select image
-(void)addImage:(id) sender
{
    
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
        [self presentViewController:picker animated:YES completion:^{
    
        }];
    

}

//Return button on Keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

//Dismiss keyboard
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //put code for store image
    [self dismissViewControllerAnimated:picker completion:^{
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
