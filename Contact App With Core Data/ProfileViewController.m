//
//  ProfileViewController.m
//  Contact App With Core Data
//
//  Created by Alex Brown on 7/9/15.
//  Copyright (c) 2015 NInjas Inc. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView* whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:whiteView];
    
    UIBarButtonItem* editBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(edit:)];
    
    self.navigationItem.rightBarButtonItem = editBtn;
    
    UILabel* profileTitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 75, self.view.frame.size.width - 20, 50)];
    profileTitleLbl.textAlignment = NSTextAlignmentCenter;
    profileTitleLbl.font = [profileTitleLbl.font fontWithSize:30];
    profileTitleLbl.text = @"PROFILE";
    [self.view addSubview:profileTitleLbl];
    
    UILabel* nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 135, self.view.frame.size.width - 20, 50)];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    nameLbl.font = [nameLbl.font fontWithSize:30];
    [self.view addSubview:nameLbl];
    
    UIButton* phoneNumberBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [phoneNumberBtn addTarget:self
                         action:@selector(Message:)
               forControlEvents:UIControlEventTouchUpInside];
    [phoneNumberBtn setTitle:@"Text" forState:UIControlStateNormal];
    phoneNumberBtn.frame = CGRectMake(10, 195, self.view.frame.size.width - 20, 50);
    [self.view addSubview:phoneNumberBtn];
    
    UIButton* phoneNumber2Btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [phoneNumber2Btn addTarget:self
                       action:@selector(call:)
             forControlEvents:UIControlEventTouchUpInside];
    [phoneNumber2Btn setTitle:@"Text" forState:UIControlStateNormal];
    phoneNumber2Btn.frame = CGRectMake(10, 255, self.view.frame.size.width - 20, 50);
    [self.view addSubview:phoneNumber2Btn];
    
    UIButton* emailBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [emailBtn addTarget:self
                       action:@selector(email:)
             forControlEvents:UIControlEventTouchUpInside];
    [emailBtn setTitle:@"Email" forState:UIControlStateNormal];
    emailBtn.frame = CGRectMake(10, 315, self.view.frame.size.width - 20, 50);
    [self.view addSubview:emailBtn];
  
    UIButton* mapsBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [mapsBtn addTarget:self
                 action:@selector(maps:)
       forControlEvents:UIControlEventTouchUpInside];
    [mapsBtn setTitle:@"Address" forState:UIControlStateNormal];
    mapsBtn.frame = CGRectMake(10, 375, self.view.frame.size.width - 20, 50);
    [self.view addSubview:mapsBtn];
    
    UIImageView* proflieImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 435, self.view.frame.size.width, 300)];
    proflieImg.contentMode = UIViewContentModeScaleAspectFit;
    //[proflieImg setImage: [UIImage imageNamed: [self.infoDictionary objectForKey:@"image"]]];
    [self.view addSubview:proflieImg];
 
}

//Text
-(void)Message:(id) sender
{
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body = @"SMS message here";
            controller.recipients = [NSArray arrayWithObjects:@"8017250192", nil];
            controller.messageComposeDelegate = self;
            [self presentViewController:controller animated:YES completion:^{
    
            }];
        }
}

//Call
-(void)call:(id) sender
{
    NSString *cleanedString = [[[infoArray objectAtIndex:3] componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSURL *telURL = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", cleanedString]];
    [[UIApplication sharedApplication] openURL:telURL];
}

//Email
-(void)email:(id) sender
{
  
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"My Subject"];
        [controller setMessageBody:@"Hello there." isHTML:NO];
        //[controller setToRecipients:@[[object valueForKey:@"email"]]];
        [self presentViewController:controller animated:YES completion:^{
            
        }];
    
}

-(void)maps:(id) sender
{
    MKMapView* map = [[MKMapView alloc]initWithFrame:self.view.bounds];
    map.delegate = self;
    //    map.showsUserLocation = YES;
    [self.view addSubview:map];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:@"4040 Harrison Blvd, Ogden Utah" completionHandler:^(NSArray* placemarks, NSError* error){
        for (CLPlacemark* aPlacemark in placemarks)
        {
            // Process the placemark.
            //            NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
            //            NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            [annotation setCoordinate:aPlacemark.location.coordinate];
            [annotation setTitle:@"WSU"]; //You can set the subtitle too
            [map addAnnotation:annotation];
            
            MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
            region.center.latitude = aPlacemark.location.coordinate.latitude ;
            region.center.longitude = aPlacemark.location.coordinate.longitude;
            region.span.longitudeDelta = 0.009f;
            region.span.latitudeDelta = 0.009f;
            [map setRegion:region animated:YES];
        }
    }];
    
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    // Use one or the other, not both. Depending on what you put in info.plist
    [self.locationManager requestWhenInUseAuthorization];
    //    [self.locationManager requestAlwaysAuthorization];
    
    [self.locationManager startUpdatingLocation];
}


-(void)edit:(id) sender
{
    
    AddOrEditViewController* editProfile = [[AddOrEditViewController alloc] init];
    [self.navigationController pushViewController:editProfile animated:YES];
    
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
